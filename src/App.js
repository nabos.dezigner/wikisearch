import React from 'react';
import WikiContainer from './components/main/WikiContainer';
import styles from './App.css';

const App = () => {
  return (
    <main>
      <WikiContainer />
    </main>
  );
};

export default App;
