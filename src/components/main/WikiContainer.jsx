import React, {useState} from 'react';
import { api } from '../api/api';
import Wiki from './Wiki';

const WikiContainer = () => {
  const [result, setResult] = useState(undefined);

  const handleClick = async (query) => {
    const response = await api.search(query);
    setResult(response);
  };

  return <Wiki data={result} search={handleClick}/>;
};

export default WikiContainer;
