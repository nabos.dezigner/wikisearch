import React, { useState } from 'react'
import styles from './wiki.module.css';

const Wiki = ({data,search}) => {
    const [input, setInput] = useState('');

    const handleSearch = (e) => {
      e.preventDefault();
      search(input);
    };

    const dataStyle = data
      ? styles.dataSearch
      : styles.onlySearch;

    return (
      <div className={dataStyle}>
        <form onSubmit={handleSearch}>
            <input
              className={styles.input}
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
            <button
              className={styles.button}
            >
              поиск на вики
            </button>
        </form>
        {data && <ResponseWiki data={data} />}
      </div>
    )
  };

  const ResponseWiki = ({data}) => {
  if (data.error) return <span>{data.error.info}</span>
    return (
        <div className={styles.data}>
          <span>Ваш запрос: <b>{data[0]}</b> </span>
          <div className={styles.dataElement}>
            {data[1].map((title, i)=>{ return (
              <div key={i}>
                <p>{title}</p>
                <p>{data[2][i]}</p>
                <a href={data[3][i]}>{data[3][i]}</a>
              </div>)
            })}
          </div>
        </div>
      )
  };
  
export default Wiki;