import axios from 'axios-jsonp-pro';

const request = async (query) => {
    const wiki = 'https://ru.wikipedia.org/w/api.php';
    const param = '?action=opensearch&format=json&origin=*&search=';
    return await axios.jsonp(wiki+param+query);
  };

export const api = {
    search(query) {return request(query)}
}